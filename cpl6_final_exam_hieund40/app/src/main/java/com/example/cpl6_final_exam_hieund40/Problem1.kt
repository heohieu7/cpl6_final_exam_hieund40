package com.example.cpl6_final_exam_hieund40

fun palindrome(input: String): Boolean {
    val sb = StringBuilder(input.trim())

    //reverse the string
    val reverseStr = sb.reverse().toString()

    //compare reversed string with input string
    return input.trim().equals(reverseStr, ignoreCase = true)
}

fun main() {
    println(palindrome("abba"))
    println(palindrome("abcdefg"))
    println(palindrome("abba "))
    println(palindrome("abb a"))
}
