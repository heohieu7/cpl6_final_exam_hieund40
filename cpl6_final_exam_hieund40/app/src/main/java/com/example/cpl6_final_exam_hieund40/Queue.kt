package com.example.cpl6_final_exam_hieund40

interface Queue<T> {

    fun enqueue(element: T): T

    fun dequeue(): T?

    val count: Int


    val isEmpty: Boolean
        get() = count == 0

    fun peek(): T?
    class Queue1<T> : Queue<T> {

        private val list = arrayListOf<T>()
        override val count: Int
            get() = list.size

        override val isEmpty: Boolean
            get() = super.isEmpty

        override fun peek(): T? = list.getOrNull(0)
        override fun enqueue(element: T): T {
            list.add(element)
            return element
        }

        override fun dequeue(): T? =
            if (isEmpty) null else list.removeAt(0)

        override fun toString(): String = list.toString()
    }
}

fun main() {
    //Test 1
    val q = Queue.Queue1<Int>()
    println(q.enqueue(1))
    println(q.dequeue())
    println(q.dequeue())

    //Test 2
    var q1 = Queue.Queue1<Char>()
    println(q1.isEmpty)
    println(q1.enqueue('A'))
    println(q1.isEmpty)
    println(q1.enqueue('B'))
    println(q1.enqueue('C'))
    println(q1.dequeue())
    println(q1.peek())
    println(q1.peek())
    println(q1.dequeue())
    println(q1.dequeue())
    println(q1.dequeue())
}