package com.example.cpl6_final_exam_hieund40


interface Stack {

    class Stack1<T> : Stack {
        var size: Int = 0

        constructor(size: Int) : this()

        constructor()

        private val storage = arrayListOf<T>()

        override fun toString() = buildString {
            appendln("----top----")
            storage.asReversed().forEach {
                appendln("$it")
            }
            appendln("-----------")
        }

        fun put(element: T): T {
            storage.add(element)
            return element
        }

        fun pop(): T? {
            if (storage.size == 0) {
                return null
            }
            return storage.removeAt(storage.size - 1)
        }

        fun peek(): T? {
            return storage.lastOrNull()
        }

        val getSize: Int
            get() = storage.size

    }

}

fun main() {
    //val s : Stack = Stack.Stack1<Int>()
    val s = Stack.Stack1<Int>()
    println(s.put(1))
    println(s.put(2))
    println(s.pop())
    println(s.pop())
    println(s.pop())
}